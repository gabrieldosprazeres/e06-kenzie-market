# Kenzie Market

## Documentação

#

### Objetivo

Kenzie Market, API criada para gerenciar um e-commerce, desde a criação dos produtos até o fechamento do carrinho do cliente para pagamento.

#

## Link da API hospedada no Heroku

- [Kenzie Market](https://e06-kenzie-market-api.herokuapp.com/).

#

## Instalação

- Primeiro faça o fork deste [repositório](https://gitlab.com/gabrieldosprazeres/e06-kenzie-market).

- Em seguida faça um git clone para a sua maquina

- Instale as dependencias necessárias utilizando o comando

```
$ yarn install
```

- Crie o container no docker

```
$ docker-compose up
```

- Crie as tabelas no banco de dados através do comando

```
$ yarn typeorm migration:run
```

- Configure suas variáveis seguindo o `.env.example`

- Inicie a aplicação local através do comando

```
$ yarn run dev
```

- A aplicação inicializará na rota http://localhost:3000/. Você deverá ver algo semelhante ao snippet logo abaixo no seu terminal:

```
yarn run v1.22.17
warning ../../../../../../package.json: No license field
$ ts-node-dev src/server.ts
[INFO] 21:55:49 ts-node-dev ver. 1.1.8 (using ts-node ver. 9.1.1, typescript ver. 4.5.5)
Running at http:localhost:3000
query: SELECT * FROM current_schema()
query: CREATE EXTENSION IF NOT EXISTS "uuid-ossp"
query: SHOW server_version;
Database Connected
```

#

## Tecnologias Utilizadas:

- TypeScript
- NodeJS
- Express
- TypeORM
- Postgres
- Docker (docker-compose)

#

## Projeto desenvolvido por:

- [Gabriel dos Prazeres](https://www.linkedin.com/in/gabrieldosprazeres/)
