export const protectedReturn = (data: any) => {
  const { user, ...otherdata } = data;
  const { code, password, ...otherUserfields } = user;

  const newData = { ...otherdata, user: otherUserfields };
  return newData;
};
