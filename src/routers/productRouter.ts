import { Router } from "express";
import { CreateProductController } from "../controllers/productController/createProductController";
import { ListOneProductController } from "../controllers/productController/listOneProductController";
import { ListProductsController } from "../controllers/productController/listProductsController";
import { isAdmUser } from "../middlewares/adminMiddleware";
import { isAuthenticated } from "../middlewares/authenticationMiddleware";
import { isAuthorizated } from "../middlewares/authorizationMiddleware";
import { validateDataSchema } from "../middlewares/validateDataSchema";
import { productSchema } from "../schemas/productSchema";

const router = Router();

export const productRouter = () => {
  router.post(
    "",
    isAuthenticated,
    isAdmUser,
    isAuthorizated,
    validateDataSchema(productSchema),
    CreateProductController
  );

  router.get("", ListProductsController);

  router.get("/:id", ListOneProductController);

  return router;
};
