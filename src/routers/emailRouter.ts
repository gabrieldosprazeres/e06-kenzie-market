import { Router } from "express";
import {
  EmailController,
  handlePasswordCode,
} from "../controllers/emailController/emailController";
import { isAdmUser } from "../middlewares/adminMiddleware";
import { isAuthenticated } from "../middlewares/authenticationMiddleware";
import { isAuthorizated } from "../middlewares/authorizationMiddleware";
import { validateDataSchema } from "../middlewares/validateDataSchema";
import { EmailSchema } from "../schemas/emailSchema";

const router = Router();

export const emailRouter = () => {
  router.post(
    "/email",
    isAuthenticated,
    isAdmUser,
    isAuthorizated,
    validateDataSchema(EmailSchema),
    EmailController
  );

  router.post("/recuperar", isAuthenticated, handlePasswordCode);

  return router;
};
