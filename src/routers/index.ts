import { Express } from "express";
import { cartRouter } from "./cartRouter";
import { emailRouter } from "./emailRouter";
import { productRouter } from "./productRouter";
import { purchaseRouter } from "./purchaseRouter";
import { userRouter } from "./userRouter";

export const initializerRouter = (app: Express) => {
  app.use("/user", userRouter());
  app.use("/product", productRouter());
  app.use("/cart", cartRouter());
  app.use("/buy", purchaseRouter());
  app.use("", emailRouter());
};
