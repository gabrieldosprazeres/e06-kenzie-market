import { Router } from "express";
import { AddItemCartController } from "../controllers/cartController/addProductController";
import { ListCartController } from "../controllers/cartController/listCartsController";
import { RemoveProductController } from "../controllers/cartController/removeProductController";
import { SelectCartController } from "../controllers/cartController/selectCartController";
import { isAdmUser } from "../middlewares/adminMiddleware";
import { isAuthenticated } from "../middlewares/authenticationMiddleware";
import { isAuthorizated } from "../middlewares/authorizationMiddleware";
import { validateDataSchema } from "../middlewares/validateDataSchema";
import { cartSchema } from "../schemas/cartSchema";
import { removeProductSchema } from "../schemas/removeProductSchema";

const router = Router();

export const cartRouter = () => {
  router.post(
    "",
    isAuthenticated,
    isAdmUser,
    isAuthorizated,
    validateDataSchema(cartSchema),
    AddItemCartController
  );

  router.delete(
    "/:product_id",
    isAuthenticated,
    isAdmUser,
    isAuthorizated,
    validateDataSchema(removeProductSchema),
    RemoveProductController
  );

  router.get(
    "",
    isAuthenticated,
    isAdmUser,
    isAuthorizated,
    ListCartController
  );

  router.get(
    "/:id",
    isAuthenticated,
    isAdmUser,
    isAuthorizated,
    SelectCartController
  );

  return router;
};
