import { Router } from "express";
import { FinishBuyController } from "../controllers/purchaseConstroller/finishBuyController";
import { ListBuyController } from "../controllers/purchaseConstroller/listBuysController";
import { SelecBuyController } from "../controllers/purchaseConstroller/selectBuyController";
import { isAdmUser } from "../middlewares/adminMiddleware";
import { isAuthenticated } from "../middlewares/authenticationMiddleware";
import { isAuthorizated } from "../middlewares/authorizationMiddleware";

const router = Router();

export const purchaseRouter = () => {
  router.post("", isAuthenticated, FinishBuyController);

  router.get(
    "/:id",
    isAuthenticated,
    isAdmUser,
    isAuthorizated,
    SelecBuyController
  );

  router.get("", isAuthenticated, isAdmUser, isAuthorizated, ListBuyController);

  return router;
};
