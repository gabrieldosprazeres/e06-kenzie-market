import { Router } from "express";
import { CreateUserController } from "../controllers/userController/createUserController";
import { DeleteUserController } from "../controllers/userController/deleteUserController";
import { ListUsersController } from "../controllers/userController/listUsersController";
import { LoginController } from "../controllers/userController/loginController";
import { UserProfileController } from "../controllers/userController/profileController";
import {
  changePassword,
  UpdateUserController,
} from "../controllers/userController/updateUserController";
import { isAdmUser } from "../middlewares/adminMiddleware";
import { isAuthenticated } from "../middlewares/authenticationMiddleware";
import { isAuthorizated } from "../middlewares/authorizationMiddleware";
import { validateDataSchema } from "../middlewares/validateDataSchema";
import { updatePasswordSchema } from "../schemas/changePasswordSchema";
import { loginSchema } from "../schemas/loginSchema";
import { userSchema } from "../schemas/userSchema";
import { updateSchema } from "../schemas/userUpdateSchema";

const router = Router();

export const userRouter = () => {
  router.post("", validateDataSchema(userSchema), CreateUserController);

  router.post("/login", validateDataSchema(loginSchema), LoginController);

  router.get(
    "",
    isAuthenticated,
    isAdmUser,
    isAuthorizated,
    ListUsersController
  );

  router.get("/:uuid", isAuthenticated, UserProfileController);

  router.patch(
    "/:uuid",
    isAuthenticated,
    isAdmUser,
    isAuthorizated,
    validateDataSchema(updateSchema),
    UpdateUserController
  );

  router.delete(
    "/:uuid",
    isAuthenticated,
    isAdmUser,
    isAuthorizated,
    DeleteUserController
  );

  router.post(
    "/alterar_senha",
    isAuthenticated,
    validateDataSchema(updatePasswordSchema),
    changePassword
  );

  return router;
};
