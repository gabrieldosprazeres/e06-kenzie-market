import { getCustomRepository } from "typeorm";
import { PurchaseRepository } from "../../repositories/purchaseRepository";
import { protectedReturn } from "../../utils/utils";

export const ListBuyService = async () => {
  const buyRepository = getCustomRepository(PurchaseRepository);
  const buy = await buyRepository.find({ relations: ["user", "products"] });

  const list = buy.map((item) => {
    return protectedReturn(item);
  });
  return list;
};
