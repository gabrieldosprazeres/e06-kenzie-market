import { getCustomRepository } from "typeorm";
import { AppError } from "../../errors";
import { PurchaseRepository } from "../../repositories/purchaseRepository";
import { protectedReturn } from "../../utils/utils";

export const SelectBuyService = async (
  buyId: number,
  uuid: string,
  isAdmin: boolean
) => {
  const buyRepository = getCustomRepository(PurchaseRepository);
  const buy = await buyRepository.findOne({
    where: { id: buyId },
    relations: ["user", "products"],
  });

  if (!buy) {
    throw new AppError("Purchase not Found", 404);
  }

  if (buy.user.uuid !== uuid || !isAdmin) {
    throw new AppError("Unauthorized", 401);
  }

  const buyResponse = protectedReturn(buy);
  return buyResponse;
};
