import { getCustomRepository } from "typeorm";
import { PurchaseRepository } from "../../repositories/purchaseRepository";
import { CartRepository } from "../../repositories/cartRepository";
import { UserRepository } from "../../repositories/userRepository";
import { AppError } from "../../errors";

export const FinishBuyService = async (uuid: string) => {
  const userRepository = getCustomRepository(UserRepository);
  const cartRepository = getCustomRepository(CartRepository);
  const purchaseRepository = getCustomRepository(PurchaseRepository);

  const user = await userRepository.findOne({ uuid });
  if (!user) {
    throw new AppError("User not Found", 404);
  }

  const cart = await cartRepository.findOne({
    where: { user },
    relations: ["products"],
  });

  if (!cart) {
    throw new AppError("Cart not Found", 404);
  }

  const data = { user, products: cart.products };
  const buy = purchaseRepository.create(data);

  cart.products = [];

  await purchaseRepository.save(buy);
  await cartRepository.save(cart);

  return buy;
};
