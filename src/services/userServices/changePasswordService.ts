import { UserRepository } from "../../repositories/userRepository";
import { getCustomRepository } from "typeorm";
import bcrypt from "bcryptjs";
import { AppError } from "../../errors";

interface Iprops {
  uuid: string;
  password: string;
  code: string;
  confirm: boolean;
}

export const UpdatePasswordService = async (data: Iprops) => {
  const usersRepository = getCustomRepository(UserRepository);

  const user = await usersRepository.findOne({ uuid: data.uuid });

  if (!user) {
    throw new AppError("user not found", 404);
  }

  if (data.code !== user.code) {
    throw new AppError("Invalid Code", 401);
  } else if (!data.confirm) {
    throw new AppError("Not confirmed", 401);
  }

  const newPassword = bcrypt.hashSync(data.password as string, 10);

  await usersRepository.update(data.uuid, {
    password: newPassword,
  });

  const { password, ...dataWithoutPassword } = user;
  return data.password;
};
