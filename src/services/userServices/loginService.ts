import { UserRepository } from "../../repositories/userRepository";
import { getCustomRepository } from "typeorm";
import bcrypt from "bcryptjs";
import { AppError } from "../../errors";
import jwt from "jsonwebtoken";

export const LoginService = async (email: string, password: string) => {
  const userRepository = getCustomRepository(UserRepository);
  const user = await userRepository.findOne({ email });

  if (!user) {
    throw new AppError("User not registered", 404);
  } else {
    const match = bcrypt.compareSync(password, user.password);
    if (!match) {
      throw new AppError("User and password missmatch.", 401);
    }
  }

  const token = jwt.sign(
    { uuid: user.uuid, email: user.email },
    process.env.JWT_SECRET as string,
    { expiresIn: "1d" }
  );
  return token;
};
