import { UserRepository } from "../../repositories/userRepository";
import { getCustomRepository } from "typeorm";

export const ListUserService = async () => {
  const usersRepository = getCustomRepository(UserRepository);

  const users = await usersRepository.find();
  const userReturn = users.map((item) => {
    const { code, password, ...otherData } = item;
    return otherData;
  });

  return userReturn;
};
