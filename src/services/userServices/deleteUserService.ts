import { UserRepository } from "../../repositories/userRepository";
import { getCustomRepository } from "typeorm";

export const DeleteUserService = async (uuid: string) => {
  const usersRepository = getCustomRepository(UserRepository);
  await usersRepository.delete(uuid);

  return "User deleted with success";
};
