import { UserRepository } from "../../repositories/userRepository";
import { getCustomRepository } from "typeorm";
import { AppError } from "../../errors";

interface IUserProps {
  name: string;
  email: string;
  password: string;
  isAdmin: boolean;
}

export const CreateUserService = async ({
  email,
  name,
  password,
  isAdmin,
}: IUserProps) => {
  const usersRepository = getCustomRepository(UserRepository);

  const emailAlreadyExists = await usersRepository.findOne({ email });

  if (emailAlreadyExists) {
    throw new AppError("E-mail already registered", 404);
  }

  const user = usersRepository.create({ name, email, isAdmin, password });

  await usersRepository.save(user);

  return user;
};
