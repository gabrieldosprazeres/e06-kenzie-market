import { UserRepository } from "../../repositories/userRepository";
import { getCustomRepository } from "typeorm";
import { AppError } from "../../errors";

export const UserProfileService = async (uuid: string) => {
  const userRepository = getCustomRepository(UserRepository);

  const user = await userRepository.findOne({ uuid });

  if (!user) {
    throw new AppError("User not found", 404);
  }

  return user;
};
