import { UserRepository } from "../../repositories/userRepository";
import { getCustomRepository } from "typeorm";
import { AppError } from "../../errors";

interface IUserUpdateProps {
  uuid: string;
  data: {
    name?: string;
    password?: string;
    email?: string;
  };
}

export const UpdateUserService = async ({ uuid, data }: IUserUpdateProps) => {
  const usersRepository = getCustomRepository(UserRepository);

  const user = await usersRepository.findOne({ uuid });

  if (user) {
    await usersRepository.update(uuid, data);
    const { password, ...dataWithoutPassword } = user;
    return dataWithoutPassword;
  }
  return new AppError("User not found", 404);
};
