import { ProductRepository } from "../../repositories/productRepository";
import { getCustomRepository } from "typeorm";
import { AppError } from "../../errors";

export const OneProductService = async (id: number) => {
  const productRepository = getCustomRepository(ProductRepository);

  const product = await productRepository.findOne({ id });

  if (!product) {
    throw new AppError("Product not found", 404);
  }

  return product;
};
