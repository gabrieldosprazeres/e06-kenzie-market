import { getCustomRepository } from "typeorm";
import { ProductRepository } from "../../repositories/productRepository";
import { AppError } from "../../errors";

interface IProps {
  name: string;
  price: number;
}

export const CreateProductService = async ({ name, price }: IProps) => {
  const productRepository = getCustomRepository(ProductRepository);

  const productAlreadyExists = await productRepository.findOne({ name });

  if (productAlreadyExists) {
    throw new AppError("Product already registered", 400);
  }

  const product = productRepository.create({ name, price });

  await productRepository.save(product);

  return product;
};
