import { getCustomRepository } from "typeorm";
import { ProductRepository } from "../../repositories/productRepository";

export const ListProductsService = async () => {
  const productRepository = getCustomRepository(ProductRepository);

  const products = productRepository.find();

  return products;
};
