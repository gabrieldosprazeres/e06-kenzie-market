import { getCustomRepository } from "typeorm";
import { CartRepository } from "../../repositories/cartRepository";
import { UserRepository } from "../../repositories/userRepository";
import { ProductRepository } from "../../repositories/productRepository";
import { protectedReturn } from "../../utils/utils";
import { AppError } from "../../errors";

interface IProduct {
  id: number;
  name: string;
  price: number;
}

interface IProps {
  uuid: string;
  product: IProduct;
}

export const AddItemCartService = async (data: IProps) => {
  const userRepository = getCustomRepository(UserRepository);
  const cartUser = await userRepository.findOne({ uuid: data.uuid });
  if (!cartUser) {
    throw new AppError("User not found!", 404);
  }

  const productRepository = getCustomRepository(ProductRepository);
  const selectedProduct = await productRepository.findOne({
    id: data.product.id,
  });

  if (!selectedProduct) {
    throw new AppError("Product not found!", 404);
  }

  const cartRepository = getCustomRepository(CartRepository);
  let cart = await cartRepository.findOne({
    where: { user: cartUser },
    relations: ["products", "user"],
  });

  if (!cart) {
    cart = cartRepository.create({
      user: cartUser,
      products: [selectedProduct],
    });
    await cartRepository.save(cart);
  } else {
    cart.products = [...cart.products, selectedProduct];

    await cartRepository.save(cart);
  }

  const finalPrice = cart.products.reduce((acumulador, item) => {
    return acumulador + item.price;
  }, 0);

  const cartReturn = protectedReturn(cart);
  return { cart: cartReturn, preco_total: finalPrice };
};
