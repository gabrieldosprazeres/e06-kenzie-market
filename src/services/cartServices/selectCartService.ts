import { getCustomRepository } from "typeorm";
import { CartRepository } from "../../repositories/cartRepository";
import { protectedReturn } from "../../utils/utils";
import { AppError } from "../../errors";

export const SelectCartService = async (
  id: number,
  uuid: string,
  isAdmin: boolean
) => {
  const cartRepository = getCustomRepository(CartRepository);
  const cart = await cartRepository.findOne({
    where: { id },
    relations: ["products", "user"],
  });

  if (!cart) {
    throw new AppError("Cart not Found", 404);
  }

  if (cart.user.uuid !== uuid || !isAdmin) {
    throw new AppError("Unauthorized", 401);
  }

  const cartResponse = protectedReturn(cart);

  return cartResponse;
};
