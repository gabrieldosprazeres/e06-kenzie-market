import { CartRepository } from "../../repositories/cartRepository";
import { getCustomRepository } from "typeorm";
import { AppError } from "../../errors";

interface IProps {
  productId: number;
  cartId: number;
  isAdmin: boolean;
  uuid: string;
}

export const RemoveProductService = async ({
  productId,
  cartId,
  uuid,
  isAdmin,
}: IProps) => {
  const cartRepository = getCustomRepository(CartRepository);
  const cart = await cartRepository.findOne({
    where: { id: cartId },
    relations: ["user", "products"],
  });

  if (!cart) {
    throw new AppError("Cart not Found", 404);
  } else if (cart.user.uuid !== uuid || !isAdmin) {
    throw new AppError("Unauthorized", 401);
  }

  const newProductsList = cart.products.filter((item) => {
    return item.id !== productId;
  });

  cart.products = newProductsList;

  await cartRepository.save(cart);

  return cart;
};
