import { getCustomRepository } from "typeorm";
import { CartRepository } from "../../repositories/cartRepository";
import { protectedReturn } from "../../utils/utils";

export const ListCartsService = async () => {
  const cartRepository = getCustomRepository(CartRepository);
  const carts = await cartRepository.find({
    relations: ["products", "user"],
  });

  const cartList = carts.map((item) => {
    const newItem = protectedReturn(item);
    return newItem;
  });

  return cartList;
};
