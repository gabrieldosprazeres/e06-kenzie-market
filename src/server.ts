import { app } from "./app";

const PORT = 3000;

app.listen(process.env.PORT || PORT, () => {
  console.log(
    `Server is running in http://localhost:${
      process.env.PORT ? process.env.PORT : PORT
    }`
  );
});
