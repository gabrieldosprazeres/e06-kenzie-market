import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
  ManyToOne,
  CreateDateColumn,
} from "typeorm";

import { Product } from ".";
import { User } from ".";

@Entity("purchase")
export default class Purchase {
  @PrimaryGeneratedColumn()
  id!: number;

  @CreateDateColumn()
  purchasedOn!: Date;

  @ManyToOne(() => User, (user) => user.purchases)
  user!: User;

  @ManyToMany(() => Product)
  @JoinTable()
  products!: Product[];
}
