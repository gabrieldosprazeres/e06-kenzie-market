import {
  Entity,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  ManyToMany,
  JoinTable,
} from "typeorm";
import { User } from ".";
import { Product } from ".";

@Entity("cart")
export default class Cart {
  @PrimaryGeneratedColumn()
  id!: number;

  @ManyToMany(() => Product, { cascade: true })
  @JoinTable()
  products!: Product[];

  @OneToOne(() => User)
  @JoinColumn()
  user!: User;
}
