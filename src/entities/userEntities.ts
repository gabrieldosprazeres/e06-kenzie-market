import bcrypt from "bcryptjs";
import { v4 as uuidv4 } from "uuid";
import { Purchase } from ".";

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
  OneToMany,
} from "typeorm";

@Entity("users")
export default class User {
  @PrimaryGeneratedColumn("uuid")
  uuid!: string;

  @Column()
  name!: string;

  @Column()
  email!: string;

  @Column()
  isAdmin!: boolean;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;

  @Column()
  password!: string;

  @Column()
  code!: string;

  @BeforeInsert()
  Code() {
    this.code = uuidv4();
  }

  @BeforeInsert()
  hashPassword() {
    this.password = bcrypt.hashSync(this.password, 10);
  }

  @OneToMany(() => Purchase, (purchase) => purchase.user)
  purchases!: Purchase[];
}
