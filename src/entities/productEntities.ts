import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity("product")
export default class Product {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name!: string;

  @Column({ type: "float", default: 0.0 })
  price!: number;
}
