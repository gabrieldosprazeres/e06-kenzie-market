import { NextFunction, Request, Response } from "express";
import { AnyObjectSchema } from "yup";
import { AppError } from "../errors";

export const validateDataSchema =
  (schema: AnyObjectSchema) =>
  async (request: Request, response: Response, next: NextFunction) => {
    const data = request.body;

    try {
      await schema.validate(data, { abortEarly: false, stripUnknown: true });
      return next();
    } catch (err) {
      return next(
        new AppError({ [(err as any).name]: (err as any).errors }, 400)
      );
    }
  };
