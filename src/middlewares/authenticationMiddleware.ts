import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import { AppError } from "../errors";

export const isAuthenticated = (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  if (!request.headers.authorization) {
    return next(new AppError("Missing authorization headers", 401));
  } else {
    const token = request.headers.authorization.split(" ")[1];

    jwt.verify(
      token as string,
      process.env.JWT_SECRET as string,
      (err: any, decoded: any) => {
        if (err) {
          return next(err);
        }
        const userUuid = decoded.uuid;
        const email = decoded.email;
        request.user = { uuid: userUuid, email: email };
      }
    );
  }
  next();
};
