import { NextFunction, Request, Response } from "express";
import { UserRepository } from "../repositories/userRepository";
import { getCustomRepository } from "typeorm";
import { AppError } from "../errors";

export const isAdmUser = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  const uuid = request.user?.uuid;
  const userRepository = getCustomRepository(UserRepository);
  const user = await userRepository.findOne({ uuid });

  if (!user) {
    return next(new AppError("User not Found", 404));
  }
  request.user = { ...request.user, isAdmin: user.isAdmin };
  next();
};
