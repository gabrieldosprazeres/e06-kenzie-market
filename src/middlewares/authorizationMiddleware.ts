import { NextFunction, Request, Response } from "express";
import { AppError } from "../errors";

export const isAuthorizated = (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  if (request.user?.isAdmin) {
    return next();
  }
  const { uuid } = request.params;
  if (request.user?.uuid !== uuid) {
    return next(new AppError("Missing admin permissions", 401));
  }
  next();
};
