import * as yup from "yup";

export const userSchema = yup.object().shape({
  name: yup.string().required("Username obrigatory"),
  email: yup.string().email("Wrong email type").required("Email obrigatory"),
  isAdmin: yup.bool(),
  password: yup.string().min(4).required("Password obrigatory"),
});
