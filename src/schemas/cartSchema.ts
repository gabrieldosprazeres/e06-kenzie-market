import * as yup from "yup";

export const cartSchema = yup.object().shape({
  id: yup.number().required(),
  name: yup.string().required(),
  price: yup.number().required(),
});
