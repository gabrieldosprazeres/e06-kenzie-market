import * as yup from "yup";

export const removeProductSchema = yup.object().shape({
  cartId: yup.number().required(),
});
