import * as yup from "yup";

export const EmailSchema = yup.object().shape({
  to: yup.string().email().required(),
  subject: yup.string().required(),
  text: yup.string().required(),
});
