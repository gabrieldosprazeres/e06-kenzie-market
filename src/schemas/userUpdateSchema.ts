import * as yup from "yup";

export const updateSchema = yup.object().shape({
  name: yup.string(),
  email: yup.string().email(),
});
