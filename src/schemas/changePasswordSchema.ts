import * as yup from "yup";

export const updatePasswordSchema = yup.object().shape({
  password: yup.string().min(4).required(),
  code: yup.string().required(),
  confirm: yup.boolean().required(),
});
