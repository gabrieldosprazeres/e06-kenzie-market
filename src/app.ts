import "reflect-metadata";
import express from "express";
import { initializerRouter } from "./routers";
import { connectDatabase } from "./database";
import { errorHandler } from "./middlewares/handleErrors";

connectDatabase();

export const app = express();

app.use(express.json());

initializerRouter(app);

app.use(errorHandler);
