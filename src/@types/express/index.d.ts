import { Request } from "express";

declare global {
  namespace Express {
    interface Request {
      user?: {
        uuid?: string;
        email?: string;
        isAdmin?: boolean;
      };
    }
  }
}
