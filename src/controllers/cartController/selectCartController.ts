import { Request, Response, NextFunction } from "express";
import { SelectCartService } from "../../services/cartServices/selectCartService";

export const SelectCartController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const uuid = request.user?.uuid as string;
    const isAdmin = request.user?.isAdmin as boolean;
    const id = parseInt(request.params.id);

    const cartResponse = await SelectCartService(id, uuid, isAdmin);

    return response.status(200).json(cartResponse);
  } catch (err) {
    next(err);
  }
};
