import { Request, Response, NextFunction } from "express";
import { RemoveProductService } from "../../services/cartServices/removeProductService";
import { protectedReturn } from "../../utils/utils";

export const RemoveProductController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const uuid = request.user?.uuid as string;
    const isAdmin = request.user?.isAdmin as boolean;
    const productId = parseInt(request.params.product_id);
    const cartId = parseInt(request.body.cartId);

    const cartResponse = await RemoveProductService({
      productId,
      cartId,
      uuid,
      isAdmin,
    });

    const cartReturn = protectedReturn(cartResponse);
    return response.status(200).json(cartReturn);
  } catch (err) {
    next(err);
  }
};
