import { ListCartsService } from "../../services/cartServices/listCartsService";
import { Request, Response, NextFunction } from "express";
import { AppError } from "../../errors";

export const ListCartController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const isAdmin = request.user?.isAdmin as boolean;
    if (!isAdmin) {
      throw new AppError("Unauthorized", 401);
    }

    const cartResponse = await ListCartsService();

    return response.status(200).json(cartResponse);
  } catch (err) {
    next(err);
  }
};
