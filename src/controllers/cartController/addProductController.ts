import { Request, Response, NextFunction } from "express";
import { AddItemCartService } from "../../services/cartServices/addProductService";

interface IProduct {
  id: number;
  name: string;
  price: number;
}

export const AddItemCartController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const uuid = request.user?.uuid as string;
    const product: IProduct = request.body;

    const cartResponse = await AddItemCartService({ uuid, product });

    return response.status(201).json(cartResponse);
  } catch (err) {
    next(err);
  }
};
