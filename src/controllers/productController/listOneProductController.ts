import { NextFunction, Response, Request } from "express";
import { OneProductService } from "../../services/productServices/listOneProductService";

export const ListOneProductController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const id = parseInt(request.params.id);

    const product = await OneProductService(id);

    return response.status(200).json(product);
  } catch (err) {
    next(err);
  }
};
