import { Request, Response, NextFunction } from "express";
import { CreateProductService } from "../../services/productServices/createProductService";

export const CreateProductController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const product = await CreateProductService(request.body);

    return response.status(201).json(product);
  } catch (err) {
    next(err);
  }
};
