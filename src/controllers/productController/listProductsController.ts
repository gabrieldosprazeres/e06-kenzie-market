import { Request, Response, NextFunction } from "express";
import { ListProductsService } from "../../services/productServices/listProductsService";

export const ListProductsController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  const products = await ListProductsService();

  return response.status(200).json(products);
};
