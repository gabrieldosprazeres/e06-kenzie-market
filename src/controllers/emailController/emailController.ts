import { NextFunction, Response, Request } from "express";
import {
  transport,
  mailTemplateOptions,
} from "../../services/emailServices/emailService";
import { UserRepository } from "../../repositories/userRepository";
import { getCustomRepository } from "typeorm";
import { v4 as uuidv4 } from "uuid";
import { AppError } from "../../errors";

export const EmailController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  const fromEmail = request.user?.email || "no-repply@mail.com";
  const { to, subject, text } = request.body;

  const userRepository = getCustomRepository(UserRepository);
  const user = await userRepository.findOne({ email: to });

  if (!user) {
    return next(new AppError("User not found", 404));
  }

  const options = mailTemplateOptions(fromEmail, to, subject, "email", {
    user: user.name,
    message: text,
  });

  transport.sendMail(options, function (err, info) {
    if (err) return next(err);
  });

  return response.status(200).json({ message: "email enviado" });
};

export const handlePasswordCode = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  const uuid = request.user?.uuid;
  const userRepository = getCustomRepository(UserRepository);
  const user = await userRepository.findOne({ uuid });

  if (!user) {
    return next(new AppError("User not found", 404));
  }

  const newcode = uuidv4();

  await userRepository.update(uuid as string, {
    code: newcode,
  });

  const fromEmail = "no-repply@mail.com";
  const subject = "Mudança de senha";
  const msg = `Seu código para mudança de senha é: ${newcode}`;

  const options = mailTemplateOptions(fromEmail, user.email, subject, "email", {
    user: user.name,
    message: msg,
  });

  transport.sendMail(options, function (err, info) {
    if (err) return next(err);
  });
  return response
    .status(200)
    .json({ message: "Código enviado para o email", code: newcode });
};
