import { Request, Response, NextFunction } from "express";
import { AppError } from "../../errors";
import { ListBuyService } from "../../services/purchaseServices/listbuysService";

export const ListBuyController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const isAdmin = request.user?.isAdmin as boolean;
    if (!isAdmin) {
      throw new AppError("Unauthorized", 401);
    }

    const buyResponse = await ListBuyService();

    return response.status(201).json(buyResponse);
  } catch (err) {
    next(err);
  }
};
