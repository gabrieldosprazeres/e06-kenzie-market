import { Request, Response, NextFunction } from "express";
import { SelectBuyService } from "../../services/purchaseServices/selectbuyService";

export const SelecBuyController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const uuid = request.user?.uuid as string;
    const isAdmin = request.user?.isAdmin as boolean;

    const id = parseInt(request.params.id);

    const buyResponse = await SelectBuyService(id, uuid, isAdmin);

    return response.status(201).json(buyResponse);
  } catch (err) {
    next(err);
  }
};
