import { Request, Response, NextFunction } from "express";
import { FinishBuyService } from "../../services/purchaseServices/finishBuyService";
import {
  transport,
  mailTemplateOptions,
} from "../../services/emailServices/emailService";
import { protectedReturn } from "../../utils/utils";

export const FinishBuyController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const uuid = request.user?.uuid as string;

    const buyResponse = await FinishBuyService(uuid);

    const listProducts = buyResponse.products.map((item) => {
      let price = item.price.toLocaleString("pt-BR", {
        style: "currency",
        currency: "BRL",
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      });

      return `${item.name} : ${price}`;
    });

    const message = listProducts.join();
    const from = "no-repply@mail.com";
    const subject = "Suas compras foram realizadas";

    const options = mailTemplateOptions(
      from,
      buyResponse.user.email,
      subject,
      "buy",
      {
        name: buyResponse.user.name,
        message: message,
        purchaseOn: buyResponse.purchasedOn,
      }
    );

    transport.sendMail(options, function (err, info) {
      if (err) return next(err);
    });

    const buyReturn = protectedReturn(buyResponse);

    return response.status(201).json(buyReturn);
  } catch (err) {
    next(err);
  }
};
