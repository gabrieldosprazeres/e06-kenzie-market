import { Request, Response, NextFunction } from "express";
import { AppError } from "../../errors";
import { ListUserService } from "../../services/userServices/listUserService";

export const ListUsersController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  if (!request.user?.isAdmin) return next(new AppError("Unauthorized", 401));

  const users = await ListUserService();

  return response.status(200).json(users);
};
