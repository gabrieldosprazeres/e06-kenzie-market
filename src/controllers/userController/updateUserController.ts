import { Request, Response, NextFunction } from "express";
import { UpdateUserService } from "../../services/userServices/updateUserService";
import { UpdatePasswordService } from "../../services/userServices/changePasswordService";

export const UpdateUserController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const { uuid } = request.params;
    const { isAdm, ...data } = request.body;

    const user = await UpdateUserService({ uuid, data });

    return response.status(200).json(user);
  } catch (err) {
    next(err);
  }
};

export const changePassword = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const uuid = request.user?.uuid;
    const data = { ...request.body, uuid };

    const newPassword = await UpdatePasswordService(data);

    return response.status(200).json({ password: newPassword });
  } catch (err) {
    next(err);
  }
};
