import { NextFunction, Response, Request } from "express";
import { UserProfileService } from "../../services/userServices/profileService";

export const UserProfileController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const uuid = request.user?.uuid;
    const user = await UserProfileService(uuid as string);

    const { password, ...dataWithoutPassword } = user;

    return response.status(200).json(dataWithoutPassword);
  } catch (err) {
    next(err);
  }
};
