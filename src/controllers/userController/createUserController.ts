import { Request, Response, NextFunction } from "express";
import { CreateUserService } from "../../services/userServices/createUserService";

export const CreateUserController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const user = await CreateUserService(request.body);
    const { password, ...dataWithoutPassword } = user;
    return response.status(201).json(dataWithoutPassword);
  } catch (err) {
    next(err);
  }
};
