import { Request, Response } from "express";
import { DeleteUserService } from "../../services/userServices/deleteUserService";

export const DeleteUserController = async (
  request: Request,
  response: Response
) => {
  const { uuid } = request.params;
  const message = await DeleteUserService(uuid);

  return response.json({ message: message });
};
