import { Request, Response, NextFunction } from "express";

import { LoginService } from "../../services/userServices/loginService";

export const LoginController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const { email, password } = request.body;

    const token = await LoginService(email, password);

    return response.status(200).json({ token: token });
  } catch (err) {
    next(err);
  }
};
